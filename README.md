# Geospatial Docker

- A Docker container that attempts to mirror the "Geospatial Processing" Singularity container [here](https://gitlab.oit.duke.edu/chart-consortium/geospatial/geospatial-processing-singularity), used to run the CI/CD pipeline to build the joined SDOH datasets in the [Prepare SDOH Data repo](https://gitlab.oit.duke.edu/chart-consortium/prepare-sdoh-data). 

- Both this container and the Singularity are built on the rocker/geospatial:latest
