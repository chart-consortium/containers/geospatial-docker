FROM rocker/geospatial:latest

############
# R packages
############

# General packages
RUN Rscript -e 'install.packages(pkgs = c("foreach", \
                                          "doParallel", \
                                          "reshape2", \
                                          "ggpattern", \
                                          "ggrepel", \
                                          "magick", \
                                          "flextable", \
                                          "emmeans", \
                                          "viridisLite", \
                                          "table1", \
                                          "here", \
                                          "kableExtra" \
                                ), \
                                quiet = TRUE, \
                                dependencies=TRUE, \
                                clean = TRUE \                     
  )'

# Spatial packages 
RUN Rscript -e 'install.packages(pkgs = c("tigris", \
                                          "tidycensus", \
                                          "acs", \
                                          "CARBayes", \
                                          "spBayes", \
                                          "rnaturalearth", \
                                          "rnaturalearthdata", \
                                          "googleway" \
                                ), \
                                quiet = TRUE, \
                                dependencies=TRUE, \
                                clean = TRUE \
  )'

